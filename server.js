var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Job = require('./api/models/jobs_model'),
  bodyParser = require('body-parser');
  
// <------- MONGO RELATED ----------->
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://localhost/JobsDb'); 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// <------- ROUTING SECTION ---------> 
var routes = require('./api/routes/router'); 
routes(app);

// <------- STARTING SERVER --------->
console.log('todo list RESTful API server started on: ' + port);
app.listen(port);
