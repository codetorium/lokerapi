'use strict';
module.exports = function(app) {
    //=================== JOB RELATED ===========================
    var jobs_controller = require('../controllers/jobs_controller');
    app.route('/jobs')
        .get(jobs_controller.list_jobs)
        .post(jobs_controller.create_job);

    app.route('/search_jobs')
        .get(jobs_controller.search_jobs);

   //==================== FILTER RELATED ========================
   var filter_controller = require('../controllers/filter_controller');
   app.route('/filter')
        .get(filter_controller.get_available_filter);    
}