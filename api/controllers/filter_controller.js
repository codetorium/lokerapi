'use strict';

var mongoose = require('mongoose'),
    Job = mongoose.model('Job'),
    flatMap = require('array.prototype.flatmap');;

exports.get_available_filter = function(req, res) {
    console.info(`Request on /filter`);

    // <---------- FILTER RESPONSE ------------>
    var filterResponse = [{
            "name": "title",
            "value": "Judul",
            "input_value": "TEXT"
        }, {
            "name": "male",
            "value": "Pria",
            "input_value": "BOOLEAN",
            "hint": "Aktifkan jika ingin mencari lowongan untuk pria"
        }, {
            "name": "female",
            "value": "Wanita",
            "input_value": "BOOLEAN",
            "hint": "Aktifkan jika ingin mencari lowongan untuk wanita"
        }, { 
            "name": "max_age",
            "value": "Umur",
            "input_value": "NUMBER",
            "hint": "Masukkan umur"
        }, {
            "name": "is_bumn",
            "value": "Milik Negeri",
            "input_value": "BOOLEAN",
            "hint": "Aktifkan jika ingin mencari lowongan perusahaan BUMN atau PNS"
        }, {
            "name": "is_swasta",
            "value": "Milik Swasta",
            "input_value": "BOOLEAN",
            "hint": "Aktifkan jika ingin mencari lowongan perusahaan Swasta"
        }];
    
    const fillDistinctValue = async () => {
        await Job.find()
        .distinct('location', function(err, data) {
            console.info(`distinct location: ${data}`)
            filterResponse.push({
                "name": "location",
                "value": "Lokasi",
                "input_value": "MULTITEXT",
                "suggested_value": data
            })   
        })
        .distinct('job_position', function(err, data) {
            console.info(`distinct position: ${data.map(position => position.job_title)}`)
            filterResponse.push({
                "name": "position",
                "value": "Posisi",
                "input_value": "MULTITEXT",
                "suggested_value": data.map(position => position.job_title)
            })

            var fields = flatMap(data.map(position => position.supported_field), x => { return x })
                    .filter((value, index, arr) => {
                        console.info(`${value} in ${arr}`)
                        return arr.indexOf(value) == index;
                    })

            filterResponse.push({
                "name": "supported_field",
                "value": "Jurusan",
                "input_value": "MULTITEXT",
                "suggested_value": fields
            })
        })

        res.status(200)
            .json(filterResponse)
    }

    fillDistinctValue()
}    
