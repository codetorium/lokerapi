'use strict';

var mongoose = require('mongoose'),
    Job = mongoose.model('Job');
    
exports.list_jobs = function(req, res) {
    var per_page = Number(req.query.per_page) || 10;
    var page = Number(req.query.page) || 1;

    console.info(`Request on /job page : ${page} item per page : ${per_page}`);

    Job.find({})
        .sort('-created_date')
        .skip((per_page * page) - per_page)
        .limit(per_page)
        .exec(function(err, job) {
            if (err)
                res.send(err);
            res.json(job);
    });
};

exports.search_jobs = function(req, res) {
    var per_page = Number(req.query.per_page) || 10;
    var page = Number(req.query.page) || 1;

    var _query = {};
    (req.query.title)           ? (_query.title = new RegExp(req.query.title, "i")) : "";
    (req.query.description)     ? (_query.description = req.query.description) : "";
    (req.query.is_bumn)         ? (_query.is_bumn = req.query.is_bumn) : "";
    (req.query.is_swasta)       ? (_query.is_swasta = req.query.is_swasta) : "";
    (req.query.male)            ? (_query.male = req.query.male) : "";
    (req.query.female)          ? (_query.female = req.query.female) : "";
    (req.query.max_age)         ? (_query.max_age = req.query.max_age) : "";
    (req.query.location)        ? (_query.location = req.query.location) : "";
    (req.query.job_position)    ? (_query.job_position = req.query.job_position) : "";
 
    console.info(`Request on /search_jobs page : ${page} item per page : ${per_page}`)

    Job.find(_query)
        .skip((per_page * page) - per_page)
        .limit(per_page)
        .exec(function(err, job) {
            if (err) res.send(err);
            res.json(job);
        });
};

exports.create_job = function(req, res) {
	var job = new Job(req.body);
	job.save(function(err, _job) {
		if (err)
			res.send(err);
		res.json(_job);
	});
};

// exports.read_a_task = function(req, res) {
// 	Task.findById(req.params.taskId, function(err, task) {
// 		if (er)
// 			res.send(err);
// 		res.json(task);
// 	});
// };

// exports.update_a_task = function(req, res) {
// 	Task.findOneAndUpdate({_id: req.params.taskId}, req.body, {new: true}, function(err, task) {
// 		if (err)
// 			res.send(err);
// 		res.json(task);
// 	});
// };

// exports.delete_a_task = function(req, res) {
// 	Task.remove({
// 		_id: req.params.taskId
// 	}, function(err, task) {
// 		if (err)
// 			res.send(err);
// 		res.json({ message: 'Task succesfully deleted' });
// 	});
//};