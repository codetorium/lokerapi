'use strict';

module.exports = {
    FILTER_TYPE : {
        BOOLEAN : 0,
        TEXT: 1,
        MULTITEXT: -1,
        NUMBER: 2,
        MULTINUMBER: -2
    }
}