'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JobSchema = new Schema({
	title : {
		type: String,
		required: 'Kindly enter the name of the task'
	},
	description : {
		type: String,
		required: 'Description should not be empty'
	},
	image_url : {
		type: String,
		required: 'Image URL should not be empty'
	},
	sex : {
		type: String,
		enum: ['male', 'female', 'all']
	},
	location : String,
	job_position: {
		job_title: String,
		supported_field: [String]
	},
	job_requirement: {
		type: String
	},
	ipk_min: {
		type: Number
	},
	height_min: [{
		gender: String,
		height: Number
	}],
	max_age: {
		type: Number
	},
	period: {
		start: Date,
		end: Date
	},
	is_bumn: Boolean,
	is_swasta: Boolean,
	created_date: {
		type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model('Job', JobSchema)