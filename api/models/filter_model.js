'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FilterSchema = new Schema({
    name: String,
    params: String,
    input_value: Number
});

module.exports = mongoose.model('Filter', FilterSchema)